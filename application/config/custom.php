<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Session Name
|--------------------------------------------------------------------------
|
*/
$config['session_name'] = 'usr_template_v3';


/*
|--------------------------------------------------------------------------
| Session Name
|--------------------------------------------------------------------------
|
*/
$config['theme'] = [
    // tampil di bagian atas sidebar
    'app_name'  => 'Backoffice LP3',
    // tampil di beranda dan lainnya
    'app_logo'  => 'img/logo-unsrat.png',
    // tampil di login
    'login_app_name' => '<strong>LP3</strong> Layanan Daring',
    // tampil di tab browser
    'website_title' => 'Layanan Daring LP3 UNSRAT | Universitas Sam Ratulangi',
    // tampil di footer layout
    'website_footer' => '<strong>Copyright &copy; ' . date('Y') . ' UPT-TIK UNSRAT</strong> * All rights reserved.',
    // nomor versi aplikasi (tampil di kanan footer)
    'version_number' => '1.0.0'
];


/*
|--------------------------------------------------------------------------
| Profiler 
|--------------------------------------------------------------------------
|
*/
$config['enable_profiler'] = FALSE;
