<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Adminlte extends MY_Controller
{

    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('templates/navbar');
        $this->load->view('adminlte_v');
        $this->load->view('templates/footer');
    }
}
