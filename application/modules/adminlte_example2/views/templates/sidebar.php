<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
        <img src="<?= base_url(); ?>resources/adminlte/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">PT. BSA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?= base_url() ?>assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p class="text">Dashboard</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Purchases
                            <i class="fas fa-angle-left right"></i>

                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ml-4 has-treeview">
                            <a href="<?= base_url('purchase/purchcopra'); ?>" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p> Copra</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="<?= base_url('purchase/purchgoods'); ?>" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Goods</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-balance-scale"></i>
                        <p>
                            Sales
                            <i class="fas fa-angle-left right"></i>

                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?= base_url('sales/salescopra'); ?>" class="nav-link">
                                <i class="fas fa-star"></i>
                                <p>Copra</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('sales/salesgoods'); ?>" class="nav-link">
                                <i class="fas fa-shopping-basket"></i>
                                <p>Goods</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-book-open"></i>
                        <p>
                            Reports
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item ml-4">
                            <a href="pages/UI/general.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>P / L</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/icons.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Cashflow</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/buttons.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Buku Kas</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/sliders.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Buku Bank</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/modals.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Stok</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/navbar.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Penjualan</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/timeline.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Pembelian</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/ribbons.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Data Pabrik</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/ribbons.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Expenses</p>
                            </a>
                        </li>
                        <li class="nav-item ml-4">
                            <a href="pages/UI/ribbons.html" class="nav-link">
                                <i class="fas fa-minus nav-icon"></i>
                                <p>Assets</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-university"></i>
                        <p>
                            Bank
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="pages/forms/general.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New Bank</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('bank/viewbank') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Bank</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('bank/paymoney') ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Pay Money</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/forms/validation.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Transfer Funds</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/forms/validation.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Receive Money</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-clipboard"></i>
                        <p>
                            Assets
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="pages/tables/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New Assets</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/data.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Assets</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>
                            Expenses
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="pages/tables/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New Expenses</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/data.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Expenses</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-id-card"></i>
                        <p>
                            Contact
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?= base_url('contact/addcontact'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New Contact</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?= base_url('contact/viewcontact'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Contact</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-layer-group"></i>
                        <p>
                            Stock
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="pages/tables/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New Stock Copra</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/data.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Stock Copra</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New Stock Goods</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/data.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage Stock Goods</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Settings
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="pages/tables/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add New User</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/data.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Manage User</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/simple.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Price Setting</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="pages/tables/data.html" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Transaction Log</p>
                            </a>
                        </li>

                    </ul>
                </li>
                <hr>

                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p class="text">Log Out</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>