<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Purchase Goods';

        $this->load->view('templates/header');
        // $this->load->view('templates/sidebar');
        $this->load->view('dashboard', $data);
        $this->load->view('templates/footer');
    }
}
