<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid bg-dark">
            <h1 class="pb-5 pt-4 text-white"><?= $title; ?></h1>
            <div class="row">
                <div class="col mb-4 d-flex flex-row-reverse">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary ml-3" data-toggle="modal" data-target="#createNewPurchase"><i class="fa fa-plus-square"></i>
                        Create New Purchase
                    </button>
                    <!-- <select id="kota" name="kota" class="form-control">
                        <option value=""></option>
                        <option value="Jakarta">Jakarta</option>
                        <option value="Bogor">Bogor</option>
                        <option value="Depok">Depok</option>
                        <option value="Tangerang">Tangerang</option>
                        <option value="Bekasi">Bekasi</option>
                        <option value="Bandung">Bandung</option>
                        <option value="Semarang">Semarang</option>
                        <option value="Yogyakarta">Yogyakarta</option>
                        <option value="Surabaya">Surabaya</option>
                    </select> -->

                    <!-- Modal Create New Purchase -->
                    <div class="content-wrapper">
                        <div class="modal fade" id="createNewPurchase" tabindex="-1" role="dialog" aria-labelledby="createNewPurchaseTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-warning">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Create New Purchase</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="vendor"><b>Vendor</b></label>
                                                        <select class="form-control" id="vendor" name="vendor">
                                                            <option selected disabled class="text-muted">Pilih Vendor...</option>
                                                            <option value="Minyak">Minyak</option>
                                                            <option value="Gas">Gas</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="customer"><b>Customer</b></label>
                                                        <select class="form-control" id="customer" name="customer">
                                                            <option selected disabled class="text-muted">Pilih Customer...</option>
                                                            <option value="Yaulie Rindengan">Yaulie Rindengan</option>
                                                            <option value="Hans Wowor">Hans Wowor</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="trans_date"><b>Transaction Date</b></label>
                                                        <input type="date" class="form-control" id="trans_date" name="trans_date">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="due_date"><b>Due Date</b></label>
                                                        <input type="date" class="form-control" id="due_date" name="due_date">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="term"><b>Term</b></label>
                                                        <input type="text" class="form-control" id="term" name="term">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="warehouse"><b>Warehouse</b></label>
                                                        <select class="form-control" id="warehouse" name="warehouse">
                                                            <option selected disabled class="text-muted">Pilih Warehouse...</option>
                                                            <option value="Minyak">Minyak</option>
                                                            <option value="Gas">Gas</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="no_transc"><b>No. Transaction</b></label>
                                                        <input type="text" class="form-control" id="no_transc" name="no_transc">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-3">
                                                    <label for="product"><b>Product</b></label>
                                                    <select class="form-control" id="product" name="product">
                                                        <option selected disabled class="text-muted">Pilih Product...</option>
                                                        <option value="Minyak">Minyak</option>
                                                        <option value="Gas">Gas</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-2">
                                                    <label for="desc"><b>Description</b></label>
                                                    <input type="text" class="form-control" id="desc" name="description">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="qty"><b>Qty</b></label>
                                                    <input type="number" class="form-control" min="0" name="qty">
                                                </div>
                                                <div class="form-group col-2">
                                                    <label for="unit"><b>Unit</b></label>
                                                    <input type="text" class="form-control" id="unit" name="unit">
                                                </div>
                                                <div class="form-group col-2">
                                                    <label for="unit_price"><b>Unit Price</b></label>
                                                    <input type="text" class="form-control" id="unit_price" name="unit_price">
                                                </div>
                                                <div class="form-group col">
                                                    <label for="amount"><b>Amount</b></label>
                                                    <input type="text" class="form-control" id="amount" name="amount">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-8">
                                                    <div class="form-group">
                                                        <a class="" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"><i class="fas fa-fw fa-plus"></i><label>Add More Data</label>
                                                        </a>
                                                        <div class="collapse" id="collapseExample">
                                                            <div class="card-header">
                                                                <h3 class="card-title">
                                                                    Memo
                                                                </h3>
                                                                <div class="card-tools">
                                                                </div>
                                                            </div>
                                                            <div class="card-body pad">
                                                                <div class="mb-3">
                                                                    <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="memo"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="file">
                                                                    <b>Attachment :</b>
                                                                </label>
                                                                <div class="input-group ml-3">
                                                                    <div class="custom-file">
                                                                        <input type="file" class="custom-file-input" id="file" name="file">
                                                                        <label class="custom-file-label" for="file">Choose file</label>
                                                                    </div>
                                                                    <div class="ml-2">
                                                                        <p class="text-danger">
                                                                            <small>
                                                                                *Max. 2 MB
                                                                            </small>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <div class="form-group">
                                                        <label for="Total"><b>Total</b></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Advance"><b>Advance</b></label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Balance Due"><b>Balance Due</b></label>
                                                    </div>
                                                </div>
                                                <div class="col-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="total" name="total" placeholder="Total">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="advance" name="advance" placeholder="Advance">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" id="balance_due" name="balance_due" placeholder="Balance Due">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary">Create Purchase</button>
                                                <button type="button" class="btn btn-dark">Purchase + Sales</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header bg-light">
                    <form>
                        <div class="row">
                            <div class="col d-flex justify-content-start">
                                <div class="form-group mr-3">
                                    <label for="openPurchase" class="text-dark d-flex justify-content-center">Open purchase</label>
                                    <input type="number" class="form-control" id="openPurchase" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="overduePurchase" class="text-dark d-flex justify-content-center">Overdue purchase</label>
                                    <input type="number" class="form-control" id="overduePurchase" placeholder="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="col-2 bg-info mb-4">
                        Purchases
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="bg-light">
                                <tr>
                                    <th>Date</th>
                                    <th>Vendor</th>
                                    <th>Due Date</th>
                                    <th>Status</th>
                                    <th>Balance</th>
                                    <th>Due</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>-</td>
                                    <td>PT. Mabes Oil</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                <tr>
                                    <td>-</td>
                                    <td>PT. Mabes Energy</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-dark mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Mabes13 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>