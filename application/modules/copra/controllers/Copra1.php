<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Copra1 extends MY_Controller
{

    public function index()
    {
        $data['title'] = 'Dashboard Copra';
        $this->load->view('templates/header');
        // $this->load->view('templates/sidebar');
        $this->load->view('dashboard1', $data);
        $this->load->view('templates/footer');
    }

    public function copra2()
    {
        $data['title'] = 'Dashboard Copra';
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('dashboard2', $data);
        $this->load->view('templates/footer');
    }
}
