<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid bg-dark">
            <h1 class="pb-5 pt-4 text-white"><?= $title; ?></h1>
            <div class="row">
                <div class="col mb-4 d-flex flex-row-reverse">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary ml-3" data-toggle="modal" data-target="#createNewPurchase"><i class="fa fa-plus-square"></i>
                        Create New Purchase
                    </button>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#createNewContract"><i class="fa fa-plus-square"></i>
                        Create New Contract
                    </button>
                    <!-- <select id="kota" name="kota" class="form-control">
                        <option value=""></option>
                        <option value="Jakarta">Jakarta</option>
                        <option value="Bogor">Bogor</option>
                        <option value="Depok">Depok</option>
                        <option value="Tangerang">Tangerang</option>
                        <option value="Bekasi">Bekasi</option>
                        <option value="Bandung">Bandung</option>
                        <option value="Semarang">Semarang</option>
                        <option value="Yogyakarta">Yogyakarta</option>
                        <option value="Surabaya">Surabaya</option>
                    </select> -->

                    <!-- Modal Create New Contract -->
                    <div class="modal fade" id="createNewContract" tabindex="-1" role="dialog" aria-labelledby="createNewContractTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-warning">
                                    <h5 class="modal-title" id="exampleModalLongTitle">
                                        Create New Contract</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form name="" method="POST" action="">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="pabrik"><b>Pabrik</b></label>
                                                        <select class="custom-select" id="pabrik" name="pabrik">
                                                            <option selected disabled class="text-muted">Pilih Pabrik...</option>
                                                            <option value="Minyak">Minyak</option>
                                                            <option value="Gas">Gas</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="trans_date"><b>Transaction Date</b></label>
                                                        <input type="date" class="form-control" id="trans_date" name="trans_date">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="amount"><b>Amount</b></label>
                                                            <input type="text" class="form-control" id="amount">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col mt-4">
                                                            <div class="form-group clearfix">
                                                                <div class="icheck-primary d-inline">
                                                                    <input type="radio" id="radioPrimary1" name="r1" checked>
                                                                    <label for="radioPrimary1"> Truck</label>
                                                                    </label>
                                                                    <input type="radio" id="radioPrimary2" name="r1">
                                                                    <label for="radioPrimary2"> Kg</label>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary float-right">Create</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal Create New Purchase -->
                    <div class="modal fade" id="createNewPurchase" tabindex="-1" role="dialog" aria-labelledby="createNewPurchaseTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-warning">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Create New Purchase</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="container-fluid">
                                                <div class="form-group mb-2">
                                                    <label for="vendor"><b>Vendor/Relasi</b></label>
                                                    <select class="custom-select" id="vendor">
                                                        <option selected disabled class="text-muted">Pilih Vendor/Relasi...</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="form-group mb-2">
                                                    <label for="pabrik"><b>Pabrik</b></label>
                                                    <select class="custom-select" id="pabrik">
                                                        <option selected disabled class="text-muted">Pilih Pabrik...</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="form-group mb-2">
                                                    <label for="date_c"><b>Date</b></label>
                                                    <input type="date" class="form-control" id="date_c">
                                                </div>
                                                <div class="form-group">
                                                    <label id="due_date"><b>Due Date</b></label>
                                                    <input type="text" class="form-control" placeholder="Due Date...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="container-fluid pb-5">
                                                <div class="row">
                                                    <div class="d-flex justify-content-start">
                                                        <label for="driver" class="text-dark d-flex justify-content-center"><b>Driver:</b></label>
                                                        <input type="text" class="form-control" id="driver">
                                                        <label for="overdueInvoices" class="text-dark d-flex justify-content-center">Plat Mobil:</label>
                                                        <input type="number" class="form-control" id="overdueInvoices" placeholder="Rp.">
                                                        <label for="openInvoices" class="text-dark d-flex justify-content-center">Bruto(kg):</label>
                                                        <input type="number" class="form-control" id="openInvoices" placeholder="Rp.">
                                                        <label for="openInvoices" class="text-dark d-flex justify-content-center">Pot(%):</label>
                                                        <input type="number" class="form-control" id="openInvoices" placeholder="Rp.">
                                                        <label for="openInvoices" class="text-dark d-flex justify-content-center">Netto(kg):</label>
                                                        <input type="number" class="form-control" id="openInvoices" placeholder="Rp.">
                                                        <label for="openInvoices" class="text-dark d-flex justify-content-center">Colly:</label>
                                                        <input type="number" class="form-control" id="openInvoices" placeholder="Rp.">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary">Create Purchase</button>
                                        <button type="button" class="btn btn-dark">Purchase + Sales</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header bg-light">
                    <form>
                        <div class="row">
                            <div class="col d-flex justify-content-start">
                                <div class="form-group mr-3">
                                    <label for="overdueInvoices" class="text-dark d-flex justify-content-center">Overdue invoices</label>
                                    <input type="number" class="form-control" id="overdueInvoices" placeholder="Rp.">
                                </div>
                                <div class="form-group">
                                    <label for="openInvoices" class="text-dark d-flex justify-content-center">Open invoices</label>
                                    <input type="number" class="form-control" id="openInvoices" placeholder="Rp.">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="col-2 bg-info mb-4">
                        Purchase Transactions
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="bg-light">
                                <tr>
                                    <th>Purchase Date</th>
                                    <th>Vendor/Relasi</th>
                                    <th>Pabrik</th>
                                    <th>Plat Mobil</th>
                                    <th>Bruto</th>
                                    <th>Netto</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                    <th>Status</th>
                                    <th>Sisa Kontrak</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2020/08/20</td>
                                    <td>PT. Mabes Oil</td>
                                    <td>AA</td>
                                    <td>DB 1111 AQ</td>
                                    <td>-</td>
                                    <td>4</td>
                                    <td>Rp. 25.000.000</td>
                                    <td>Rp. 100.000.000</td>
                                    <td>Open overdue</td>
                                    <td>2 truck</td>
                                </tr>
                                <tr>
                                    <td>2020/08/20</td>
                                    <td>PT. Mabes Energy</td>
                                    <td>BB</td>
                                    <td>DB 2222 AJ</td>
                                    <td>-</td>
                                    <td>4</td>
                                    <td>Rp. 25.000.000</td>
                                    <td>Rp. 100.000.000</td>
                                    <td>Posting</td>
                                    <td>2 ton</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="7">Sub Total: </th>
                                    <th colspan="3">Rp. 200.000.000 </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-dark mt-auto">
        <div class="container-fluid">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Mabes13 2020</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>