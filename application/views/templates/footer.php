</body>
<script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>resources/dist/js/scripts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>resources/dist/assets/demo/chart-area-demo.js"></script>
<script src="<?php echo base_url(); ?>resources/dist/assets/demo/chart-bar-demo.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
<script src="<?php echo base_url(); ?>resources/dist/assets/demo/datatables-demo.js"></script>
</script>
<script src="<?php echo base_url(); ?>resources/select2/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function() {
        $("#kota").select2({
            placeholder: "Please Select"
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var i = 1;

        $('#add').click(function() {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '" class="dynamic-added"><td><input type="text" name="addmore[][nama_bank]"  class="form-control nama_bank_list" required /></td><td><input type="text" name="addmore[][no_rek]" class="form-control no_rek_list" required /></td><td><input type="text" name="addmore[][nama_rek]" class="form-control nama_rek_list" required /></td><td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td></tr>');
        });

        $(document).on('click', '.btn_remove', function() {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();
        });

    });
</script>